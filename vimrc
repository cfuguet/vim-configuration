"  Enable Pathogen
execute pathogen#infect()
syntax on
filetype plugin indent on

"" ==============================
"" General
"" ==============================

" Activate improved features of Vim (less Vi-compatible)
set nocompatible

" Disable bells on error
set noerrorbells
set novisualbell

" Increase the undo limit
set history=1000

set autoread   " Set to auto read when a file is changed from the outside
set lazyredraw " Don't redraw while executing macros
set showmatch  " Show matching brackets

" Search settings
set ignorecase
set smartcase
set hlsearch
set incsearch
set magic " Turn magic on for regular expressions

" Turn off swap files
set noswapfile

" Set mappin leader character
let mapleader = ","
let g:mapleader = ","

" Enable modelines
set modeline
set modelines=5

" Enable mouse integration
set mouse=a

"  Allow directory-local vim script
set exrc
set secure

"" ==============================
"" Motion configuration
"" ==============================

" Treat long lines as break lines (useful when moving around in them)
map j gj
map k gk

" Use the arrows to something usefull
map <leader>l :bn<cr>
map <leader>k :bp<cr>

" Backspace behavior when in insert mode
set backspace=indent,eol,start

"" ==============================
"" VIM user interface
"" ==============================

" This makes typing Esc take effect more quickly
set ttimeout
set ttimeoutlen=50

" Turn on wild menu
set wildmenu
set wildignore=*.o,*.a,*~,*.pyc

" Status configuration
set ruler " Show current position
set number " Show line number
set cursorline " Show current cursor line
set cmdheight=1 " Height of the command bar
set laststatus=2 " Always show the status line

" Display special characters
set list "Enable list mode
set listchars=tab:>\ ,trail:-,nbsp:+

" Long lines break on word boundary (visual wrap without actual EOL)
set linebreak

"  Set color scheme
set t_Co=256
set background=dark
" set termguicolors
colorscheme solarized8
if has("gui_running")
	" Set fonts
	if has("osx")
		set guifont=Menlo:h12
	else
		set guifont=Deja\ Vu\ Sans\ Mono\ 11
	endif
endif

" Pressing ,ss will toggle and untoggle spell checking
map <leader>ss :setlocal spell!<cr>

" Quit insert mode pressing Ctrl+space
imap <C-space> <ESC>

"" ==============================
"" NERDTree
"" ==============================
map <leader>n :NERDTreeToggle<CR>

"" ==============================
"" Airline
"" ==============================
let g:airline#extensions#tabline#enabled = 1
let g:airline_right_sep=''
let g:airline_section_y=""

"" ==============================
"" Tagbar
"" ==============================
map <leader>t :TagbarToggle<CR>
let g:tagbar_autofocus=1
let g:tagbar_compact = 1
let g:tagbar_usearrows = 1
let g:tagbar_autoshowtag = 1
let g:tagbar_ctags_bin = "ctags"

"" ==============================
"" Syntastic
"" ==============================
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0
let g:syntastic_enable_signs = 1
let g:syntastic_filetype_map = { "systemverilog": "verilog" }
let g:syntastic_mode_map = { "mode": "passive" }

map <leader>cc :SyntasticCheck<cr>
map <leader>cx :SyntasticToggleMode<cr>

"" ==============================
"" Misc
"" ==============================

"  Enable the Man command to explore manpages directly from VIM
runtime ftplugin/man.vim

"  Enable the localrc Vim plugin: enable directory-wised vimrc configuration
runtime plugin/localrc.vim
